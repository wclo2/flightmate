import os
import sys
import random
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'flightmate.settings')

import django
django.setup()

from webapp.models import FlightRecords, RecordSet, Member, FlightSeg

def handleCSV(filename):
    lineRead = 0
    frid = 0
    csvFile = open(filename)
    lineList = []
    finalList = []
    for line in csvFile:
        if not line:
            break
        lineRead += 1
        if lineRead % 3 != 1:
            continue
        lineList = line.split(',')
        if lineRead == 1:
            keyList = lineList.copy()
            #print(keyList)
        else:
             for j in range(len(keyList)):
                 if j == 0:
                     dict = {keyList[j]: lineList[j]}
                 else:
                     dict.update({keyList[j]: lineList[j]})
             finalList.append(dict.copy())
        if lineRead % 1000 == 0:
            for r in finalList:
                add_records(frid, r["Year"], r["Month"], r["DayofMonth"], r["Origin"], r["Dest"], r["DepTime"],
                            r["CRSDepTime"], r["ArrTime"], r["CRSArrTime"], r["DepDelay"], r["ArrDelay"],
                            r["UniqueCarrier"], r["FlightNum"])
                frid += 1
            finalList.clear()
            print("inserted 1000 records")
    csvFile.close()


def convertTime(time):
    s = time.split(".")
    hour = ""
    minute = ""
    if len(s[0]) == 3:
        hour = s[0][:1]
        minute = s[0][1:]
    elif len(s[0]) == 4:
        hour = s[0][:2]
        minute = s[0][2:]
    else:
        hour = "00"
        minute = s[0]
    if len(s) == 1:
        return hour + ":" + minute
    else:
        return hour + ":" + minute + "." + s[1]

nameList = ["AMELIA","OLIVIA","EMILY","AVA","ISLA","JESSICA","POPPY","ISABELLA","SOPHIE","MIA","RUBY","LILY","GRACE","EVIE","SOPHIA","ELLA","SCARLETT","CHLOE","ISABELLE","FREYA","CHARLOTTE","SIENNA","DAISY","PHOEBE","MILLIE","EVA","ALICE","LUCY","FLORENCE","SOFIA","LAYLA","LOLA","HOLLY","IMOGEN","MOLLY","MATILDA","LILLY","ROSIE","ELIZABETH","ERIN","MAISIE","LEXI","ELLIE","HANNAH","EVELYN","ABIGAIL","ELSIE","SUMMER","MEGAN","JASMINE","MAYA","AMELIE","LACEY","WILLOW","EMMA","BELLA","ELEANOR","ESME","ELIZA","GEORGIA","HARRIET","GRACIE","ANNABELLE","EMILIA","AMBER","IVY","BROOKE","ROSE","ANNA","ZARA","LEAH","MOLLIE","MARTHA","FAITH","HOLLIE","AMY","BETHANY","VIOLET","KATIE","MARYAM","FRANCESCA","JULIA","MARIA","DARCEY","ISABEL","TILLY","MADDISON","VICTORIA","ISOBEL","NIAMH","SKYE","MADISON","DARCY","AISHA","BEATRICE","SARAH","ZOE","PAIGE","HEIDI","LYDIA"]
userList = []

def createUsers():

    for n in nameList:
        m = Member.objects.create(name = n, birthday = "1999-10-18", gender = "male" if random.randint(0, 1) == 1 else "female",
            email = (n + "@uiuc.edu"), password = "12345", profile = "111")
        userList.append(m)


def add_records(frid, year, month, day, origin, dest, depart_time_actual, depart_time_scheduled,
                arrival_time_actual, arrival_time_scheduled, dep_delay, arr_delay, airline, number):
    
    depart_date = "{0:04d}-{1:02d}-{2:02d}".format(int(year), int(month), int(day))
    depart_time_r = convertTime(depart_time_scheduled)[:6]
    arrival_time_r = convertTime(arrival_time_scheduled)[:6]

    if random.randint(0, 10000) == 0:
        u = Member.objects.get(email = "123@uiuc.edu")
    elif random.randint(0, 10000) == 0:
        u = Member.objects.get(email = "456@uiuc.edu")
    else:
        u = userList[random.randint(0, len(userList)-1)]

    r = FlightSeg.objects.create(origin = origin, dest = dest, 
        depart_time = (depart_date + " " + depart_time_r), arrival_time = (depart_date + " " + arrival_time_r),
        airline = airline, flight_num = number, user = u)
    r.save()
    return r

def add_set():
    attr = ["airline", "dest", "origin"]
    for a in attr:
        r = RecordSet.objects.get_or_create(name=a)[0]
        r.name = a
        objs = FlightRecords.objects.order_by(a).values(a).distinct()
        posList = []
        for o in objs:
            posList.append(o[a])
        r.value = "|".join(posList)
        r.save()
    #print(FlightRecords.objects.values('airline').distinct())

""""
def populate(filename):
    while True:
        recordList = handleCSV(filename)
        if len(recordList) == 0:
            break
        frid = 0
        numRecords = 0
        for r in recordList:
            add_records(frid, r["Year"], r["Month"], r["DayofMonth"], r["Origin"], r["Dest"], r["DepTime"],
                    r["CRSDepTime"], r["ArrTime"], r["CRSArrTime"], r["DepDelay"], r["ArrDelay"],
                    r["UniqueCarrier"], r["FlightNum"])
            frid += 1
            # print(frid)
            numRecords += 1
            if numRecords % 1000 == 0:
                print("inserted 1000 records")
    #add_set()
"""


if __name__ == '__main__':
    print("Starting populating DB...")
    createUsers()
    handleCSV(sys.argv[1])
