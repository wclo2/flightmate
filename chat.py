import tornado.web
import tornado.websocket
import tornado.httpserver
import tornado.ioloop
import socket
import sys
import _thread
from tornado.options import define, options


HOST = ''       #Symbolic name, meaning all available interfaces
PORT = 8888     #Arbitrary non-privileged port

SERVER_KEY = "329b3449-4980-4ca3-82de-39185a0d8845"

clients = dict()


class WebSocketServer(object):
    def __init__(self, port=PORT):
        # define("port", default=port, help="Run on the given port", type=int)
        app = tornado.web.Application([
        (r'/', IndexHandler),
        (r'/(?P<Id>\w*)', MyWebSocketHandler),
        (r"/static/(.*)", tornado.web.StaticFileHandler, {'path':'static/'}),
        ])
        print("DSP Control Application | Browse to <IP>:"+str(PORT))
        tornado.options.parse_command_line()
        app.listen(PORT)
        print("websocket server running")
        tornado.ioloop.IOLoop.instance().start()

class IndexHandler(tornado.web.RequestHandler):
    @tornado.web.asynchronous
    def get(self, **kwargs):
        pass

class MyWebSocketHandler(tornado.websocket.WebSocketHandler):

    def open(self, *args, **kwargs):
        self.stream.set_nodelay(True)

    def on_message(self, message):
        print("received a message: ", message)
        msg = message.split("|")
        if msg[0] == SERVER_KEY:
            #SERVER_KEY + "|" + receiver.email + "|" + receiver.name + "|" + user.email + "|" + user.name + "|" + room_id + "|" + content
            self.id = msg[0]
            recv_email = msg[1]
            recv_name = msg[2]
            send_email = msg[3]
            send_name = msg[4]
            chatid = msg[5]
            content = msg[6]
            sender = clients.get(send_email, "")
            if sender:
                print("send: ", send_name + "," + chatid + "," + content)
                sender.write_message(send_name + "," + chatid + "," + content)

            receiver = clients.get(recv_email, "")
            if receiver:
                print("send: ", recv_name + "," + chatid + "," + content)
                receiver.write_message(recv_name + "," + chatid + "," + content)
        else:
            if not clients.get(message, ""):
                print(message)
                self.id = message
                clients[self.id] = self

    def on_close(self):
        try:
            print(self.id, " close!")
        except:

            print("unknown closed")
        if self.id in clients:
            del clients[self.id]

    def check_origin(self, origin):
        return True

if __name__ == "__main__":
    server = WebSocketServer()


