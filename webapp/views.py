from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from webapp.models import FlightRecords, RecordSet, FlightSeg, Member
from webapp.models import Chatroom, ChatRelation, Message
from webapp.helper import RES_SUCCESS, RES_FAIL
from webapp.helper import updateModel, doHash, user_auth, websocketSender
from webapp.helper import FILE_PATH, SERVER_KEY
from django.contrib.sessions.backends.db import SessionStore
from django.contrib.sessions.models import Session
import json, os, datetime
from django_prepared_query import PreparedManager, BindParam

def trip(request):
    user = None
    token = request.GET.get("token", "")
    if not token:
        return JsonResponse(RES_FAIL)
    is_auth, userid = user_auth(token)
    if not is_auth:
        return JsonResponse(RES_FAIL)
    try:
        user = Member.objects.get(id=userid)
    except:
        return JsonResponse(RES_FAIL)

    if request.method == 'POST':
        json_data = request.body.decode('utf-8')
        try:
            data = json.loads(json_data)
            print(data)
        except:
            return JsonResponse(RES_FAIL)
        action = data["action"]
        del data["action"]
        if action == "insert":
            try:
                data["depart_time"] = datetime.datetime.strptime(data.get("depart_time", ""), "%Y-%m-%d %H:%M")
                data["arrival_time"] = datetime.datetime.strptime(data.get("arrival_time", ""), "%Y-%m-%d %H:%M")
                data["user"] = user
                if data.get("fsid", ""):
                    del data["fsid"]
                FlightSeg.objects.create(**data)
                return JsonResponse(RES_SUCCESS)
            except:
                return JsonResponse(RES_FAIL)
        elif action == "update":
            ID = -1
            if data.get("fsid", ""):
                ID = data["fsid"]
                del data["fsid"]
            else:
                return JsonResponse(RES_FAIL)
            if data.get("user", ""):
                del data["user"]
            try:
                if data.get("depart_time", ""):
                    data["depart_time"] = datetime.datetime.strptime(data["depart_time"], "%Y-%m-%d %H:%M")
                if data.get("arrival_time", ""):
                    data["arrival_time"] = datetime.datetime.strptime(data["arrival_time"], "%Y-%m-%d %H:%M")
                obj = FlightSeg.objects.get(fsid=ID, user__id=user.id)
                updateModel(obj ,data)
                return JsonResponse(RES_SUCCESS)
            except:
                return JsonResponse(RES_FAIL)
        elif action == "delete":
            ID = data["fsid"]
            try:
                FlightSeg.objects.get(fsid=ID, user__id=user.id).delete()
                return JsonResponse(RES_SUCCESS)
            except:
                return JsonResponse(RES_FAIL)
        else:
            return JsonResponse(RES_FAIL)
    elif request.method == 'GET':
        flights = FlightSeg.objects.filter(user__id=userid).values()
        flysegs = []
        for f in flights:
            f["companion"] = []
            compSet = set()
            #find companion
            comp = FlightSeg.objects.exclude(user__id=user.id).filter(depart_time=f["depart_time"],
                                                                        arrival_time=f["arrival_time"],
                                                                        airline=f["airline"])
            for c in comp:
                compSet.add(c.user.email)
            f["companion"] = list(compSet)
            f["depart_time"] = f["depart_time"].strftime("%Y-%m-%d %H:%M")
            f["arrival_time"] = f["arrival_time"].strftime("%Y-%m-%d %H:%M")
            flysegs.append(f)
        #print(flysegs)
        response = {}
        response["result"] = flysegs
        return JsonResponse(response)
    else:
        return JsonResponse(RES_FAIL)

def account(request):
    if request.method == 'POST':
        json_data = request.body.decode('utf-8')
        try:
            data = json.loads(json_data)
            print(data)
        except:
            return JsonResponse(RES_FAIL)
        action = data["action"]
        del data["action"]
        token = ""
        userid = -1
        user = None
        #check auth
        if action in ["logout", "edit"]:
            token = request.GET.get("token", "")
            if not token:
                return JsonResponse(RES_FAIL)
            is_auth, userid = user_auth(token)
            if not is_auth:
                return JsonResponse(RES_FAIL)

        if action == "register":
            try:
                mail = data.get("email", "")
                user = Member.objects.filter(email=mail)
                if user:
                    return JsonResponse(RES_FAIL)
                pwd = data.get("password", "")
                data["password"] = doHash(pwd)
                #print(data["password"])
                birthday = data.get("birthday", "")
                data["birthday"] = datetime.datetime.strptime(birthday, "%Y-%m-%d").date()
                Member.objects.create(**data)
                return JsonResponse(RES_SUCCESS)
            except:
                return JsonResponse(RES_FAIL)
        elif action == "login":
            mail = data.get("email", "")
            pwd = data.get("password", "")
            pwd = doHash(pwd)
            try:
                user = Member.objects.get(email=mail, password=pwd)
            except:
                return JsonResponse(RES_FAIL)
            s = SessionStore()
            s["user_id"] = user.id
            s.create()
            token = s.session_key
            return JsonResponse({"result":"success", "token":token})
        elif action == "logout":
            Session.objects.get(pk=token).delete()
            return JsonResponse(RES_SUCCESS)
        elif action == "edit":
            try:
                if data.get("password", ""):
                    data["password"] = doHash(data["password"])
                if data.get("birthday", ""):
                    data["birthday"] = datetime.datetime.strptime(data["birthday"], "%Y-%m-%d").date()
                if data.get("email", ""):
                   del data["email"]
                try:
                    user = Member.objects.get(id=userid)
                except:
                    return JsonResponse(RES_FAIL)
                print(data)
                updateModel(user, data)
                return JsonResponse(RES_SUCCESS)
            except:
                return JsonResponse(RES_FAIL)
        else:
            JsonResponse(RES_SUCCESS)
    elif request.method == 'GET':
        userid = -1
        user = None
        token = request.GET.get("token", "")
        if not token:
            return JsonResponse(RES_FAIL)
        is_auth, userid = user_auth(token)
        if not is_auth:
            return JsonResponse(RES_FAIL)
        user = Member.objects.get(id=userid)
        #ps = Member.objects.filter(id=BindParam('id'))
        #user = ps.execute(id=userid)[0]
        if not user:
            return JsonResponse(RES_FAIL)
        return JsonResponse({"name":user.name, "birthday":str(user.birthday), "gender":user.gender,
                            "email":user.email})
    else:
        return JsonResponse(RES_FAIL)

def chatroom(request):
    user = None
    token = request.GET.get("token", "")
    if not token:
        return JsonResponse(RES_FAIL)
    is_auth, userid = user_auth(token)
    if not is_auth:
        return JsonResponse(RES_FAIL)
    try:
        user = Member.objects.get(id=userid)
    except:
        return JsonResponse(RES_FAIL)
    json_data = request.body.decode('utf-8')
    try:
        data = json.loads(json_data)
        print(data)
    except:
        return JsonResponse(RES_FAIL)
    action = data["action"]
    if action == "create":
        add_member = None
        try:
            m = data["addmember"]
            add_member = Member.objects.get(email=m)
        except:
            return JsonResponse(RES_FAIL)
        chat_room = Chatroom.objects.create(numbers=2)
        ChatRelation.objects.create(user=user, room=chat_room)
        ChatRelation.objects.create(user=add_member, room=chat_room)
        return JsonResponse({"result":"success", "chatroomid":chat_room.crid})
    elif action == "show":
        relation = ChatRelation.objects.filter(user=user)
        roomInfo = []
        for r in relation:
            info = {}
            info["id"] = r.room.crid
            info["member"] = list()
            people = ChatRelation.objects.filter(room=r.room) #exclude(user=user)
            for p in people:
                info["member"].append(p.user.name)
            roomInfo.append(info)
        return JsonResponse({"result":"success", "chatroom":roomInfo})

    elif action == "delete":
        room_id = data["chatroomid"]
        room = None
        if not room_id:
            return JsonResponse(RES_FAIL)
        try:
            room = Chatroom.objects.get(crid=room_id)
        except:
            return JsonResponse(RES_FAIL)
        if room.numbers > 1:
            room.numbers = room.numbers - 1
            room.save()
        else:
            room.delete()
        return JsonResponse(RES_SUCCESS)
    else:
        return JsonResponse(RES_FAIL)

def message(request):
    user = None
    token = request.GET.get("token", "")
    if not token:
        return JsonResponse(RES_FAIL)
    is_auth, userid = user_auth(token)
    if not is_auth:
        return JsonResponse(RES_FAIL)
    try:
        user = Member.objects.get(id=userid)
    except:
        return JsonResponse(RES_FAIL)
    json_data = request.body.decode('utf-8')
    try:
        data = json.loads(json_data)
        print(data)
    except:
        return JsonResponse(RES_FAIL)
    action = data["action"]
    if action == "add":
        #cid, sender, content, time
        room_id = data["chatroomid"]
        try:
            chatRoom = Chatroom.objects.get(crid=room_id)
        except:
            return JsonResponse(RES_FAIL)
        content = data["content"]
        Message.objects.create(chatRoom=chatRoom, sender=user, content=content)
        #send to websocket server
        #get other members in chatroom, currently we support only 2 members in a room
        re = ChatRelation.objects.filter(room=chatRoom).exclude(user=user)
        receiver = re[0].user
        msgPush = SERVER_KEY + "|" + receiver.email + "|" + receiver.name + "|" + user.email + "|" + user.name + "|" + room_id + "|" + content
        websocketSender(msgPush)
        return JsonResponse(RES_SUCCESS)

    elif action == "search":
        room_id = data["chatroomid"]
        msg_nums = int(data["number"])
        time_from = data["time_from"]
        try:
            chatRoom = Chatroom.objects.get(crid=room_id)
            timeFrom = datetime.datetime.strptime(time_from, "%Y-%m-%d %H:%M")
        except:
            return JsonResponse(RES_FAIL)
        #ct = Message.objects.count()
        #if ct < msg_nums:
            #msg_nums = ct
        msgs = Message.objects.filter(chatRoom=chatRoom, time__lte=timeFrom).order_by("-time")[:msg_nums]
        res = reversed(msgs)
        msgList = []
        for m in res:
            info = {}
            info["sender"] = m.sender.name
            info["content"] = m.content
            info["time"] = m.time.strftime("%Y-%m-%d %H:%M")
            msgList.append(info)
        return JsonResponse({"result":"success", "message":msgList})

    else:
        return JsonResponse(RES_FAIL)

def picupload(request):
    token = request.GET.get("token", "")
    if not token:
        return JsonResponse(RES_FAIL)
    is_auth, userid = user_auth(token)
    if not is_auth:
        return JsonResponse(RES_FAIL)
    try:
        user = Member.objects.get(id=userid)
    except:
        return JsonResponse(RES_FAIL)
    if request.method == 'POST':
        pic_data = request.body #decode('utf-8')
        print(pic_data)
        full_path = None
        if user.profile: #delete old profile
            full_path = os.path.join(FILE_PATH, user.profile)
            try:
                os.remove(full_path)
            except:
                print("delete fail")
        user.profile = user.email + "_" + datetime.datetime.now().strftime("%Y%m%d%H%M")
        user.save()
        full_path = os.path.join(FILE_PATH, user.profile)
        with open(full_path, 'wb+') as destination:
            destination.write(pic_data)
            destination.close()
        return JsonResponse(RES_SUCCESS)
    else:
        return JsonResponse(RES_FAIL)


def index(request):
    airlines = RecordSet.objects.get(name="airline")
    departs = RecordSet.objects.get(name="origin")
    dests = RecordSet.objects.get(name="dest")
    context = {}
    context["airline"] = airlines.value.split("|")
    context["origin"] = departs.value.split("|")
    context["dest"] = dests.value.split("|")
    return render(request, 'index.html', context)

def query(request):
    print(request.POST)
    airline = request.POST['airlines']
    origin = request.POST['departs']
    dest = request.POST['dests']
    date = request.POST['depart_date']
    filters = {}
    if airline != "None":
       filters['airline'] = airline
    if origin != "None":
        filters['origin'] = origin
    if dest != "None":
        filters['dest'] = dest
    if date != "":
        filters['depart_date'] = date
    res = FlightRecords.objects.filter(**filters)

    context = {}
    context["results"] = res
    return render(request, 'query.html', context)

def mobileapp(request):
    return render(request, 'mobileapp.html')