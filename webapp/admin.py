from django.contrib import admin
from webapp.models import FlightRecords, RecordSet, Member, FlightSeg
from webapp.models import Chatroom, ChatRelation, Message

class MemberAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Member._meta.fields]

class ChatroomAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Chatroom._meta.fields]

class FlightRecordsAdmin(admin.ModelAdmin):
    list_display = [f.name for f in FlightRecords._meta.fields]

class RecordSetAdmin(admin.ModelAdmin):
    list_display = [f.name for f in RecordSet._meta.fields]

class FlightSegAdmin(admin.ModelAdmin):
    list_display = [f.name for f in FlightSeg._meta.fields]

class ChatRelationAdmin(admin.ModelAdmin):
    list_display = [f.name for f in ChatRelation._meta.fields]

class MessageAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Message._meta.fields]

admin.site.register(FlightRecords, FlightRecordsAdmin)
admin.site.register(RecordSet, RecordSetAdmin)
admin.site.register(Member, MemberAdmin)
admin.site.register(FlightSeg, FlightSegAdmin)
admin.site.register(Chatroom, ChatroomAdmin)
admin.site.register(ChatRelation, ChatRelationAdmin)
admin.site.register(Message, MessageAdmin)
