import hashlib
import socket
import os
from django.contrib.sessions.models import Session
from django.db import models
from websocket import create_connection

SERVER_IP = 'localhost'
SERVER_PORT = 8888
SERVER_KEY = "329b3449-4980-4ca3-82de-39185a0d8845"

RES_SUCCESS = {"result": "success"}
RES_FAIL = {"result": "fail"}

FILE_DIR = 'profile'
FILE_PATH = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))) + '/' + FILE_DIR


def updateModel(obj, data_dict):
    for k, v in data_dict.items():
        setattr(obj, k, v)
    obj.save()

def doHash(input):
    in_encode = input.encode('utf-8')
    hex_dig = hashlib.sha256(in_encode).hexdigest()
    return str(hex_dig)

def websocketSender(message):
    ws = create_connection("ws://127.0.0.1:8888/ws")
    ws.send(message)
    ws.close()

def tcpSender(message):
    fd = None
    try:
        fd = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        fd.connect((SERVER_IP, SERVER_PORT))
        tosend = message.encode('utf-8')
        print("message to websocket server: ", tosend)
        fd.sendall(tosend)
    except:
        print("send error")
    fd.close()

def user_auth(token):
    try:
        s = Session.objects.get(pk=token)
    except:
        return False, -1
    userid = s.get_decoded().get("user_id", -1)
    if userid > 0:
        return True, userid
    else:
        return False, -1