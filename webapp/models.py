from __future__ import unicode_literals
import uuid
import datetime
from django.db import models
from django_prepared_query import PreparedManager, BindParam

class FlightRecords(models.Model):
    frid = models.IntegerField(default=0)
    origin = models.CharField(max_length=512)
    dest = models.CharField(max_length=512)
    depart_date = models.CharField(max_length=512)
    depart_time_actual = models.CharField(max_length=512, null=True)
    depart_time_scheduled = models.CharField(max_length=512, null=True)
    arrival_time_actual = models.CharField(max_length=512, null=True)
    arrival_time_scheduled = models.CharField(max_length=512, null=True)
    dep_delay = models.FloatField(default=0.0)
    arr_delay = models.FloatField(default=0.0)
    #should be foreign key
    airline = models.CharField(max_length=512)
    number = models.IntegerField(default=0)
    def __unicode__(self):
        return "{0}-{1}-{2}-{3}-{4}".format(frid, origin, dest, depart_time, arrival_time)

class RecordSet(models.Model):
    name = models.CharField(max_length=512)
    value = models.TextField()
    def __unicode__(self):
        return "{0}: {1}".format(name, value)

#Name, Birthday, Gender, email, Password
class Member(models.Model):
    name = models.CharField(max_length=512)
    birthday = models.DateField(auto_now=False, auto_now_add=False)
    gender = models.CharField(max_length=32)
    email = models.EmailField(unique=True, db_index=True)
    password = models.CharField(max_length=512)
    profile = models.CharField(max_length=128, null=True)
    prestat = PreparedManager()
    def __str__(self):
        return str(self.name)

    def save(self, *args, **kwargs):
        if self.pk == None:
            if self.gender.lower() != "male" and self.gender.lower() != "female":
                raise ValidationError("invalid gender")
        self.full_clean()
        super(Member, self).save(*args, **kwargs)

class Chatroom(models.Model):
    crid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, db_index=True)
    numbers = models.IntegerField(default=0)
    def __str__(self):
        return str(self.crid)

class ChatRelation(models.Model):
    user = models.ForeignKey(Member, verbose_name='chat_user', on_delete=models.CASCADE)
    room = models.ForeignKey(Chatroom, verbose_name='chat_room', on_delete=models.CASCADE)
    class Meta:
        unique_together = ('user', 'room',)

class Message(models.Model):
    chatRoom = models.ForeignKey(Chatroom, verbose_name='message_room', on_delete=models.CASCADE)
    sender = models.ForeignKey(Member, verbose_name='message_sender')
    content = models.CharField(max_length=512)
    time = models.DateTimeField(auto_now_add=True, db_index=True)

#FSID, Origin, Destination, Departure Time, Arrival Time, Predicted Delay Time, Airline, Flight Number
class FlightSeg(models.Model):
    fsid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, db_index=True)
    origin = models.CharField(max_length=512)
    dest = models.CharField(max_length=512)
    depart_time = models.DateTimeField(auto_now=False, auto_now_add=False)
    arrival_time = models.DateTimeField(auto_now=False, auto_now_add=False)
    airline = models.CharField(max_length=512)
    flight_num = models.CharField(max_length=128)
    user = models.ForeignKey(Member, verbose_name='user_flightseg', on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.full_clean()
        super(FlightSeg, self).save(*args, **kwargs)

"""
class Reserved(models.Model):
    user = models.ForeignKey(User, verbose_name='user_reserved', on_delete=models.CASCADE)
    flyseg = models.ForeignKey(FlightSeg, verbose_name='flyseg_reserved', on_delete=models.CASCADE)
"""






