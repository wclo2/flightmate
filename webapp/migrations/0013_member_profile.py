# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-12-06 05:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0012_auto_20171205_1926'),
    ]

    operations = [
        migrations.AddField(
            model_name='member',
            name='profile',
            field=models.CharField(max_length=128, null=True),
        ),
    ]
