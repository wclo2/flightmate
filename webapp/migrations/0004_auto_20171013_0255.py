# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-10-13 02:55
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0003_auto_20171013_0240'),
    ]

    operations = [
        migrations.AlterField(
            model_name='flightrecords',
            name='arr_delay',
            field=models.FloatField(default=0.0),
        ),
        migrations.AlterField(
            model_name='flightrecords',
            name='dep_delay',
            field=models.FloatField(default=0.0),
        ),
    ]
