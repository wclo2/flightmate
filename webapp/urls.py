from django.conf.urls import include, url
from webapp import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^query$', views.query, name='query'),
    url(r'^mobileapp$', views.mobileapp, name='mobileapp'),
    url(r'^account$', views.account, name='account'),
    url(r'^trip$', views.trip, name='trip'),
    url(r'^picture$', views.picupload, name='picture'),
    url(r'^chatroom$', views.chatroom, name='chatroom'),
    url(r'^chatmessage$', views.message, name='chatmessage'),
]



