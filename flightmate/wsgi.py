"""
WSGI config for flightmate project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""

import os
from os.path import join,dirname,abspath
import sys

PROJECT_DIR = dirname(dirname(abspath(__file__)))
print(PROJECT_DIR)
sys.path.insert(0, PROJECT_DIR) 


#os.environ.setdefault("DJANGO_SETTINGS_MODULE", "flightmate.settings")
sys.path.append("/usr/lib64/python3.4/site-packages/")
os.environ["DJANGO_SETTINGS_MODULE"] = "flightmate.settings"

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
